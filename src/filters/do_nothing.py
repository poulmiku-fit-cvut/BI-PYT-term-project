from ._base import BaseFilter


class DoNothingFilter(BaseFilter):

    def process(self, image):
        """ No need to convert to and from np array
        """
        return image
