import scipy.misc as smp
import numpy as np


class BaseFilter:

    def convert_to_numpy(self, image):
        return np.asarray(image)

    def process(self, image):
        numpy_array = self.convert_to_numpy(image)

        res = self.process_numpy_array(numpy_array)

        return self.convert_from_numpy(res)

    def convert_from_numpy(self, numpy_array):
        return smp.toimage(numpy_array)

    def process_numpy_array(self, numpy_array):
        raise NotImplementedError
