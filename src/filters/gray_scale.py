import numpy as np
from numba import jit

from src.utils import timeit
from ._base import BaseFilter


class GrayScaleFilter(BaseFilter):

    def set_gray(self, new_array, x, y, gray):
        for i in range(3):
            new_array[y, x, i] = gray

    @timeit
    def process_numpy_array(self, numpy_array: np.array, output_shape=None):
        two_d = len(numpy_array.shape) == 2

        if two_d:
            return numpy_array

        if output_shape is None:
            output_shape = numpy_array.shape
        new_array = np.zeros(output_shape)

        for y in range(numpy_array.shape[0]):
            for x in range(numpy_array.shape[1]):
                darkness = sum(numpy_array[y, x]) // 3

                if len(output_shape) == 3:
                    self.set_gray(new_array, x, y, darkness)
                else:
                    new_array[y, x] = darkness

        return new_array
