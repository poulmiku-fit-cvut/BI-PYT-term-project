import numpy as np

from ._base import BaseFilter


class RotateFilter(BaseFilter):

    def __init__(self, direction="right", degrees=45):
        super().__init__()
        self.direction = (0, 1) if direction == "left" else (1, 0)
        self.degrees = degrees // 90

    def process_numpy_array(self, numpy_array: np.array):
        res = np.rot90(numpy_array, k=self.degrees, axes=self.direction)

        return res
