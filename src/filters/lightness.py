import numpy as np

from src.utils import timeit
from ._base import BaseFilter


class LightnessFilter(BaseFilter):

    def __init__(self, percentage):
        self.darker = -1 if percentage < 0 else 1
        self.percentage = abs(percentage) / 100

    def change_lightness(self, x):
        return max(0, min(255, round(x + self.darker * 255 * self.percentage)))

    @timeit
    def process_numpy_array(self, numpy_array: np.array):

        change_lightness = np.vectorize(self.change_lightness, otypes=[np.int])

        return change_lightness(numpy_array)
