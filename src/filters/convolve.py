import numpy as np

from src.utils import timeit
from ._base import BaseFilter


class BaseConvolveFilter(BaseFilter):

    KERNEL = None

    @timeit
    def process_numpy_array(self, numpy_array: np.array):
        return self.convolve(numpy_array.copy(),
                             self.KERNEL)

    def convolve(self, pixels, kernel):
        kernel_width, kernel_height = kernel.shape

        if len(pixels.shape) == 2:
            pixels_width, pixels_height = pixels.shape
        else:
            pixels_width, pixels_height, _ = pixels.shape

        pad = int(kernel_width/2)

        x = [[], []]
        x[0] = (pad, pad)
        x[1] = (pad, pad)
        if len(pixels.shape) == 3:
            x.append((0, 0))

        bigger = np.lib.pad(pixels, x, 'reflect').astype(np.float)

        pixels.fill(0)
        pixels = pixels.astype(np.float)

        for x in range(kernel_width):
            for y in range(kernel_height):
                pixels += kernel[x, y] * bigger[x: pixels_width + x,
                                                y: pixels_height + y]

        return pixels


class SobelFilter(BaseConvolveFilter):

    KERNEL = np.array([[-1, -1, -1],
                       [-1, 8, -1],
                       [-1, -1, -1]])


class SharpenFilter(BaseConvolveFilter):

    KERNEL = np.array([[0, -1, 0],
                       [-1, 5, -1],
                       [0, -1, 0]])


class BoxBlurFilter(BaseConvolveFilter):

    KERNEL = 1/9 * np.ones((3, 3), dtype=np.float)


class GaussianBlurFilter(BaseConvolveFilter):

    KERNEL = 1/256 * np.array([[1, 4, 6, 4, 1],
                               [4, 16, 24, 16, 4],
                               [6, 24, 36, 24, 6],
                               [4, 16, 24, 16, 4],
                               [1, 4, 6, 4, 1]], dtype=np.float)
