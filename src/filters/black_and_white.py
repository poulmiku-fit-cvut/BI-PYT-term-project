import numpy as np
from numba import jit

from src.utils import timeit
from ._base import BaseFilter


class SimpleBlackAndWhiteFilter(BaseFilter):

    def set_white(self, new_array, x, y):
        for i in range(3):
            new_array[y, x, i] = 255

    @timeit
    def process_numpy_array(self, numpy_array: np.array):
        two_d = len(numpy_array.shape) == 2
        new_array = np.zeros(numpy_array.shape)

        for y in range(numpy_array.shape[0]):
            carried_error = 0
            for x in range(numpy_array.shape[1]):
                if not two_d:
                    current_darkness = sum(numpy_array[y, x]) // 3
                else:
                    current_darkness = 255 - numpy_array[y, x]

                darkness = carried_error + 255 - current_darkness
                if darkness > 127:
                    carried_error = darkness - 255
                else:
                    carried_error = darkness
                    if not two_d:
                        self.set_white(new_array, x, y)
                    else:
                        new_array[y, x] = 255

        return new_array


class BetterBlackAndWhiteFilter(SimpleBlackAndWhiteFilter):

    @timeit
    @jit
    def process_numpy_array(self, numpy_array: np.array):
        two_d = len(numpy_array.shape) == 2
        new_array = np.zeros((numpy_array.shape[0], numpy_array.shape[1], 3))

        error_current = np.zeros(numpy_array.shape[1], dtype=int)
        error_next = np.zeros(numpy_array.shape[1], dtype=int)

        for y in range(numpy_array.shape[0]):
            for x in range(numpy_array.shape[1]):
                if not two_d:
                    current_darkness = sum(numpy_array[y, x]) // 3
                else:
                    current_darkness = 255 - numpy_array[y, x]

                darkness = int(round(error_current[x])) + 255 - current_darkness

                if darkness > 127:
                    error = darkness - 255
                else:
                    error = darkness
                    if not two_d:
                        self.set_white(new_array, x, y)
                    else:
                        new_array[y, x] = 255

                if x > 0 and x <= numpy_array.shape[1] - 3:
                    error_current[x + 1] += int(round(7 / 16 * error))
                    error_next[x + 1] += int(round(1 / 16 * error))
                    error_next[x] += int(round(5 / 16 * error))
                    error_next[x - 1] += int(round(3 / 16 * error))

            np.copyto(error_current, error_next)
            error_next.fill(0)

        return new_array
