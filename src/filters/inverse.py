import numpy as np

from ._base import BaseFilter


class InverseFilter(BaseFilter):

    def process_numpy_array(self, numpy_array: np.array):
        res = np.flip(numpy_array, axis=1)

        return res
