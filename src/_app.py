import tkinter as tk
import tkinter.filedialog
import tkinter.messagebox

from pathlib import Path
from PIL import Image, ImageTk

from src.filters import BaseFilter

PREVIEW_SIZE = 512


class App:

    def __init__(self):
        self.available_filters = {}
        self.selected_filters = {}
        self.order = None

        self.window = self.menu_frame = self.images_frame = None
        self.input_canvas = self.result_canvas = None
        self.open_button = self.save_button = self.button_frame = None
        self.input_filename = None

        self.input_image = self.input_preview = self.canvas_input_preview = None
        self.result_image = self.result_preview = self.canvas_result_preview = None

    def register_filter(self, instance, name, description):
        if not issubclass(instance.__class__, BaseFilter):
            raise ValueError("Filter has to be subclass of BaseFilter")

        self.available_filters[name] = (instance, description)

    def set_filter_order(self, order):
        self.order = order

    def get_filter(self, name):
        return self.available_filters[name][0]

    def create_window(self):
        self.window = tk.Tk()
        self.window.title("Application")

        self.create_widgets()

    def create_widgets(self):
        self.menu_frame = tk.Frame(self.window)
        self.menu_frame.grid(row=0)

        self.button_frame = tk.Frame(self.window)
        self.button_frame.grid(row=1)

        self.images_frame = tk.Frame(self.window)
        self.images_frame.grid(row=2)

        for row_id, row in enumerate(self.order):
            for column_id, filter_ in enumerate(row):
                self.selected_filters[filter_] = tk.BooleanVar()
                tk.Checkbutton(self.menu_frame,
                               text=self.available_filters[filter_][1],
                               variable=self.selected_filters[filter_],
                               command=self.on_checkboxes_change).grid(row=row_id, column=column_id)

        self.open_button = tk.Button(self.button_frame, text="Open file", command=self.on_open_button_clicked)
        self.open_button.grid(row=1, column=0)

        self.save_button = tk.Button(self.button_frame, text="Save result", command=self.on_save_button_clicked)
        self.save_button.grid(row=1, column=1)

        self.input_canvas = tk.Canvas(self.images_frame, width=PREVIEW_SIZE, height=PREVIEW_SIZE, bg="white")
        self.input_canvas.grid(row=0, column=0)
        self.result_canvas = tk.Canvas(self.images_frame, width=PREVIEW_SIZE, height=PREVIEW_SIZE, bg="white")
        self.result_canvas.grid(row=0, column=1)

    def show_error(self, error):
        tk.messagebox.showerror("Error", error)

    def remove_transparency(self, im, bg_colour=(255, 255, 255)):

        # Only process if image has transparency (http://stackoverflow.com/a/1963146)
        if im.mode in ('RGBA', 'LA') or (im.mode == 'P' and 'transparency' in im.info):

            # Need to convert to RGBA if LA format due to a bug in PIL (http://stackoverflow.com/a/1963146)
            alpha = im.convert('RGBA').split()[-1]

            # Create a new background image of our matt color.
            # Must be RGBA because paste requires both images have the same format
            # (http://stackoverflow.com/a/8720632  and  http://stackoverflow.com/a/9459208)
            bg = Image.new("RGBA", im.size, bg_colour + (255,))
            bg.paste(im, mask=alpha)

            return bg.convert("RGB")

        else:
            return im

    def set_input_image(self, filename):
        if not filename.endswith(".png"):
            self.show_error("Not a png file")
            return

        filename = Path(filename)
        try:
            self.input_image = Image.open(filename)
        except Exception as e:
            self.show_error(e)
            return

        self.input_filename = filename

        self.input_image = self.remove_transparency(self.input_image)

        self.draw_input_image()
        self.redraw_result()

    def on_open_button_clicked(self):
        ftypes = [('PNG images', '*.png'), ('All files', '*.*')]  # find out how to select just images
        dialog = tk.filedialog.Open(self.window, filetypes=ftypes)
        fl = dialog.show()

        if fl:
            self.set_input_image(fl)

    def save_result(self, filename):
        filename = Path(str(filename))

        filename.parent.mkdir(exist_ok=True, parents=True)

        self.result_image.save(filename)

    def on_save_button_clicked(self):
        if self.input_image is None:
            self.show_error("No image loaded")
            return

        fl = tk.filedialog.asksaveasfilename(defaultextension=".png",
                                             filetypes=[('PNG images', '*.png'), ('All files', '*.*')])

        if fl:
            self.save_result(fl)

    def on_checkboxes_change(self):
        self.redraw_result()

    def resize_to_preview(self, image):
        new_image = image.copy()
        new_image.thumbnail((PREVIEW_SIZE, PREVIEW_SIZE), Image.ANTIALIAS)

        return new_image

    def calculate_offset(self, image):
        return (PREVIEW_SIZE - image.width) // 2, (PREVIEW_SIZE - image.height) // 2

    def draw_image(self, image, canvas: tk.Canvas):
        if image.width > PREVIEW_SIZE or image.height > PREVIEW_SIZE:
            preview = self.resize_to_preview(image)
        else:
            preview = image

        canvas.delete("all")

        photo_image = ImageTk.PhotoImage(preview)
        x = canvas.create_image(PREVIEW_SIZE // 2, PREVIEW_SIZE // 2, image=photo_image)

        return preview, (x, photo_image)

    def draw_input_image(self):
        self.input_preview, self.canvas_input_preview = self.draw_image(self.input_image, self.input_canvas)

    def redraw_result(self):
        if self.input_image is None:
            return

        self.result_image = self.input_image.copy()

        for filter_name, value in self.selected_filters.items():
            if value.get():
                self.result_image = self.get_filter(filter_name).process(self.result_image)

        self.result_preview, self.canvas_result_preview = self.draw_image(self.result_image, self.result_canvas)

    def main_loop(self):
        tk.mainloop()
