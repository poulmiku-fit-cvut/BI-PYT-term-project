import time


def timeit(method):
    def timed(*args, **kwargs):
        ts = time.time()
        result = method(*args, **kwargs)
        te = time.time()

        print('%r %r %2.2f sec' % (method.__name__, args[0].__class__.__name__, te - ts))
        return result

    return timed
