# Term project for BI-BYT, 2017/2018

Full score.
You should probably only use this as a reference - it would be kinda... obvous.
Licensed under MIT except for the images in the img folder.

## How to run:

  1. Install Python 3.6
  2. Create a virtualenv.
  3. Install requirements (``pip install -r requirements.txt``).
  3. Run ``python main.py``.
