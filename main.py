from src import App
from src.filters import DoNothingFilter, RotateFilter, InverseFilter, SimpleBlackAndWhiteFilter, \
    BetterBlackAndWhiteFilter, GrayScaleFilter, LightnessFilter, SobelFilter, SharpenFilter, BoxBlurFilter, \
    GaussianBlurFilter

if __name__ == "__main__":
    app = App()
    app.register_filter(DoNothingFilter(), "do_nothing", "Do nothing filter")
    app.register_filter(RotateFilter(direction="left", degrees=90), "rotate_left", "Rotate left 90%")
    app.register_filter(RotateFilter(direction="right", degrees=90), "rotate_right", "Rotate right 90%")
    app.register_filter(RotateFilter(direction="right", degrees=180), "rotate", "Rotate 180%")
    app.register_filter(InverseFilter(), "inverse", "Inverse image")
    app.register_filter(GrayScaleFilter(), "gray", "Turn to gray scale")
    app.register_filter(SimpleBlackAndWhiteFilter(), "simple_black_and_white", "Simple black and white")
    app.register_filter(BetterBlackAndWhiteFilter(), "better_black_and_white", "Better black and white")
    app.register_filter(LightnessFilter(percentage=-40), "make_darker_40", "Make 40% darker")
    app.register_filter(LightnessFilter(percentage=40), "make_lighter_40", "Make 40% lighter")
    app.register_filter(LightnessFilter(percentage=-60), "make_darker_60", "Make 60% darker")
    app.register_filter(LightnessFilter(percentage=60), "make_lighter_60", "Make 60% lighter")
    app.register_filter(SobelFilter(), "sobel", "Sobel")
    app.register_filter(SharpenFilter(), "sharpen", "Sharpen")
    app.register_filter(BoxBlurFilter(), "box_blur", "Box blur")
    app.register_filter(GaussianBlurFilter(), "gaussian", "Gaussian blur")

    app.set_filter_order(
        [
            ["do_nothing", "rotate_left", "rotate_right", "rotate", "inverse"],
            ["gray", "simple_black_and_white", "better_black_and_white"],
            ["make_darker_40", "make_lighter_40", "make_darker_60", "make_lighter_60", ],
            ["sobel", "sharpen", "box_blur", "gaussian"],
        ]
    )

    app.create_window()
    app.main_loop()
